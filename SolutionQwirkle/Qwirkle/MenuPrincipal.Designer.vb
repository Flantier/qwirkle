﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMP
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMP))
        Me.lblPhraseIntro = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.cmdJouer = New System.Windows.Forms.Button()
        Me.lblCopyright = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblPhraseIntro
        '
        Me.lblPhraseIntro.AutoSize = True
        Me.lblPhraseIntro.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhraseIntro.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.lblPhraseIntro.Location = New System.Drawing.Point(308, 9)
        Me.lblPhraseIntro.Name = "lblPhraseIntro"
        Me.lblPhraseIntro.Size = New System.Drawing.Size(0, 24)
        Me.lblPhraseIntro.TabIndex = 2
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(74, 36)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(649, 168)
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'cmdJouer
        '
        Me.cmdJouer.BackColor = System.Drawing.Color.LimeGreen
        Me.cmdJouer.FlatAppearance.BorderSize = 0
        Me.cmdJouer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.ForestGreen
        Me.cmdJouer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdJouer.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdJouer.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.cmdJouer.Location = New System.Drawing.Point(265, 279)
        Me.cmdJouer.Name = "cmdJouer"
        Me.cmdJouer.Size = New System.Drawing.Size(264, 69)
        Me.cmdJouer.TabIndex = 4
        Me.cmdJouer.Text = "Jouer"
        Me.cmdJouer.UseVisualStyleBackColor = False
        '
        'lblCopyright
        '
        Me.lblCopyright.AutoSize = True
        Me.lblCopyright.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblCopyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.lblCopyright.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblCopyright.Location = New System.Drawing.Point(12, 426)
        Me.lblCopyright.Name = "lblCopyright"
        Me.lblCopyright.Size = New System.Drawing.Size(50, 17)
        Me.lblCopyright.TabIndex = 5
        Me.lblCopyright.Text = "©2019"
        '
        'FrmMP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(800, 452)
        Me.Controls.Add(Me.lblCopyright)
        Me.Controls.Add(Me.cmdJouer)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblPhraseIntro)
        Me.Name = "FrmMP"
        Me.Text = "Qwirkle - Menu Principal"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblPhraseIntro As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents cmdJouer As Button
    Friend WithEvents lblCopyright As Label
End Class

﻿Imports QwirkleLibrary

Public Class frmMEJ
    Public valid As Integer
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles cmdReprendrePartie.Click
        My.Computer.Audio.Play(Plateau.GetDirectory() & "\buttonsound.wav", AudioPlayMode.Background)
        Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles cmdRetourMenuPrincipal.Click
        My.Computer.Audio.Play(Plateau.GetDirectory() & "\buttonsound.wav", AudioPlayMode.Background)
        frmV.ShowDialog()
        If valid = 1 Then
            frmMCJ.Show()
            Close()
            frmFDJ.Close()
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles cmdRegles.Click
        My.Computer.Audio.Play(Plateau.GetDirectory() & "\buttonsound.wav", AudioPlayMode.Background)
        frmR.Show()
        Close()
    End Sub
End Class